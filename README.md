# Instalador

**Kubernetes management with support to Helm and Lighbend RP.**


Given one JSON file/URL ensure the Kubernetes cluster is compliant with the specification.

## Capabilities:
* Receives configuration JSON from POST request or by doing GET in some URL
* Keeps a set of Deployer Actors, one for each application component.
* Deployer Actors 
    * generate compliance report
    * prepare cluster for Deployment
    * may have knowledge for one specific technology (Kafka, Akka ActorSystem), and report some extra metrics
    * show update plans
    * execute update operations
    * send heartbeat to Elasticsearch

* Kibana shows dashboards

This implementation aligns with the [concepts of operators from CoreOs](https://coreos.com/operators/)

## Arquitecture

![Instalador Architecture](doc/InstaladorArchitecture.png)

## Some useful commands:

* Running the Docker container on the host machine at port 8080 (must grant privileged=true in order to access docker host api)
```bash
docker run --privileged=true --detach --name installer --publish 8080:8080 --volume /var/run/docker.sock:/var/run/docker.sock installer:0.1.0-SNAPSHOT
```

## Installing on Kubernetes with the chart provided:

### Create service account 
```bash
export NAMESPACE_OPS=ops
kubectl create namespace ${NAMESPACE_OPS}
kubectl create serviceaccount -n ${NAMESPACE_OPS} instalador-sa
kubectl create clusterrolebinding instalador-sa-cluster-admin-rule --clusterrole=cluster-admin --serviceaccount=${NAMESPACE_OPS}:instalador-sa
```

### Create Kubernetes configuration

The credentials to the Kubernetes cluster are configure for the POD as kubectl config file, the ![example](chart/prereqs/instalador-k8s-config.yaml) chart can be used.
```bash
kubectl create -n ${NAMESPACE_OPS} -f instalador-k8s-config.yaml
```

### Create registry credentials (case using private repo)
```bash
kubectl create secret docker-registry instalador-registry-secrets --docker-server=registry.gitlab.com --docker-username=*************** --docker-password=*********** --docker-email=****** -n ${NAMESPACE_OPS}
kubectl patch serviceaccount instalador-sa -p '{"imagePullSecrets": [{"name": "instalador-registry-secrets"}]}' -n ${NAMESPACE_OPS}
```

### Install chart
Changing to the `chart` folder in the root of the project and installing the chart:
```bash
helm install instalador --set serviceAccount=instalador-sa --namespace ${NAMESPACE_OPS} --name instalador-v01
```
