package org.boardcrew.installer

import akka.actor.ActorRef
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model.{ContentTypes, HttpRequest, MessageEntity, StatusCodes}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.concurrent.Eventually.eventually
import org.scalatest.{Matchers, WordSpec}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Seconds, Span}
import spray.json._

class InitalizationSpec extends WordSpec with Matchers with ScalaFutures with ScalatestRouteTest
  with ApplicationRoutes with JsonSupport {
  lazy val routes = userRoutes
  override val installation: ActorRef = system.actorOf(Installation.props, "InstallationActor")
  val installationName = "Initialization"

  "ApplicationRoutes creating the topology with phases first " should {
    "load phases (POST /phases)" in {
      val configuration = scala.io.Source.fromResource("Preparation-phases-test.json").mkString
      val phases: PhasesState = configuration.parseJson.convertTo[PhasesState]
      val configEntity = Marshal(phases).to[MessageEntity].futureValue
      val request = Post("/phases").withEntity(configEntity)
      eventually(timeout(Span(20, Seconds)), interval(Span(3, Seconds))) {
        request ~> routes ~> check {
          status should ===(StatusCodes.Created)
          contentType should ===(ContentTypes.`application/json`)
          entityAs[OperationResult].status should ===("Success")
          entityAs[OperationResult].phase should ===(s"AddPhases for deployer: Preparation")
        }
      }
    }
    "load configuration from file (POST /installation)" in {
      val configuration = scala.io.Source.fromResource("installation.json").mkString
      val installation: InstallationState = configuration.parseJson.convertTo[InstallationState]

      val configEntity = Marshal(installation).to[MessageEntity].futureValue
      val request = Post("/installation").withEntity(configEntity)

      request ~> routes ~> check {
        status should ===(StatusCodes.Created)
        contentType should ===(ContentTypes.`application/json`)
        entityAs[OperationResult].status should ===("Success")
      }
    }
  }
}
