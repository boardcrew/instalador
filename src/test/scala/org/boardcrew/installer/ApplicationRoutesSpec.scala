package org.boardcrew.installer

import akka.actor.ActorRef
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.concurrent.Eventually._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{Matchers, WordSpec}
import spray.json._

class ApplicationRoutesSpec extends WordSpec with Matchers with ScalaFutures with ScalatestRouteTest
  with ApplicationRoutes with JsonSupport {
  lazy val routes = userRoutes
  override val installation: ActorRef = system.actorOf(Installation.props, "InstallationActor")
  val installationName = "Simpletest"

//  "ApplicationRoutes creating the topology variables " should {
//        "load simple configuration (POST /installation)" in {
//          val installation: Installation = Installation(
//            installationName,
//            "0.1.0",
//            true,
//            Seq(Parameter("test","test","test")),
//            Seq(ApplicationComponent(
//              "test",
//              "test",
//              "test",
//              true,
//              Seq(),
//              Seq(),
//              Seq()))
//          )
//
//          val configEntity = Marshal(installation).to[MessageEntity].futureValue
//          val request = Post("/installation").withEntity(configEntity)
//          request ~> routes ~> check {
//            status should ===(StatusCodes.Created)
//            contentType should ===(ContentTypes.`application/json`)
//            entityAs[OperationResult].status should ===("Success")
//          }
//        }
//        "return simple configuration installation name (GET /installation)" in {
//          val request = HttpRequest(uri = "/installation")
//          request ~> routes ~> check {
//            status should ===(StatusCodes.OK)
//            contentType should ===(ContentTypes.`application/json`)
//            entityAs[Installation].name should ===(installationName)
//          }
//        }
//    }

  "ApplicationRoutes creating the topology for preparation only " should {
    "load configuration from file (POST /installation)" in {
      val configuration = scala.io.Source.fromResource("installation-preparation-only.json").mkString
      val installation: InstallationState = configuration.parseJson.convertTo[InstallationState]

      val configEntity = Marshal(installation).to[MessageEntity].futureValue
      val request = Post("/installation").withEntity(configEntity)

      request ~> routes ~> check {
        status should ===(StatusCodes.Created)
        contentType should ===(ContentTypes.`application/json`)
        entityAs[OperationResult].status should ===("Success")
      }
    }
    "return installation from file (GET /installation)" in {
      val request = HttpRequest(uri = "/installation")
      eventually(timeout(Span(10, Seconds)), interval(Span(1000, Millis))) {
        request ~> routes ~> check {
          status should ===(StatusCodes.OK)
          contentType should ===(ContentTypes.`application/json`)
          entityAs[InstallationState].name should ===("Installation preparation only")
          // Should contain the 4 application components defined in the configuration file
          entityAs[InstallationState].applicationComponents.length shouldEqual 1
        }
      }
    }
    "get application component (GET /component/Preparation)" in {
      val request = HttpRequest(uri = "/component/Preparation")
      eventually(timeout(Span(20, Seconds)), interval(Span(3, Seconds))) {
        request ~> routes ~> check {
          status should ===(StatusCodes.OK)
          contentType should ===(ContentTypes.`application/json`)
          entityAs[ApplicationComponent].name should ===("Preparation")
        }
      }
    }
    "load phases (PATCH /phases/Preparation)" in {
      val configuration = scala.io.Source.fromResource("Preparation-phases-test.json").mkString
      val phases: PhasesState = configuration.parseJson.convertTo[PhasesState]
      val configEntity = Marshal(phases).to[MessageEntity].futureValue
      val request = Patch("/phases/Preparation").withEntity(configEntity)
      eventually(timeout(Span(20, Seconds)), interval(Span(3, Seconds))) {
        request ~> routes ~> check {
          status should ===(StatusCodes.Created)
          contentType should ===(ContentTypes.`application/json`)
          entityAs[OperationResult].status should ===("Accepted")
          entityAs[OperationResult].phase should ===("Phases loaded")
        }
      }
    }
//    "get application component (GET /phases/Preparation)" in {
//      val request = HttpRequest(uri = "/phases/Preparation")
//      eventually(timeout(Span(20, Seconds)), interval(Span(3, Seconds))) {
//        request ~> routes ~> check {
//          status should ===(StatusCodes.OK)
//          contentType should ===(ContentTypes.`application/json`)
//          entityAs[ApplicationComponent].name should ===("Preparation")
//        }
//      }
//    }
  }

//  "ApplicationRoutes executing preparation commands" should {
//    "request deployer to PREPARE cluster (POST /component/Preparation)" in {
//      val request = Post("/component/Preparation")
//      request ~> routes ~> check {
//        status should ===(StatusCodes.OK)
//        contentType should ===(ContentTypes.`application/json`)
//        entityAs[OperationResult].status should ===("Queued")
//      }
//    }
//    // TODO: how to check if the preparation was performed as expected?
//  }

//  "ApplicationRoutes creating the topology for preparation and components " should {
//    "load configuration from file (POST /installation)" in {
//      val configuration = scala.io.Source.fromResource("installation.json").mkString
//      val installation: Installation = configuration.parseJson.convertTo[Installation]
//
//      val configEntity = Marshal(installation).to[MessageEntity].futureValue
//      val request = Post("/installation").withEntity(configEntity)
//
//      request ~> routes ~> check {
//        status should ===(StatusCodes.Created)
//        contentType should ===(ContentTypes.`application/json`)
//        entityAs[OperationResult].status should ===("Success")
//      }
//    }
//    "return installation from file (GET /installation)" in {
//      val request = HttpRequest(uri = "/installation")
//      eventually(timeout(Span(20, Seconds)), interval(Span(500, Millis))) {
//        request ~> routes ~> check {
//          status should ===(StatusCodes.OK)
//          contentType should ===(ContentTypes.`application/json`)
//          entityAs[Installation].name should ===("Installation from file")
//          // Should contain the 4 application components defined in the configuration file
//          entityAs[Installation].applicationComponents.length shouldEqual 4
//        }
//      }
//    }
//    "get application component (GET /component/Cassandra)" in {
//      val request = HttpRequest(uri = "/component/Cassandra")
//      eventually(timeout(Span(10, Seconds)), interval(Span(500, Millis))) {
//        request ~> routes ~> check {
//          status should ===(StatusCodes.OK)
//          contentType should ===(ContentTypes.`application/json`)
//          entityAs[ApplicationComponent].name should ===("Cassandra")
//        }
//      }
//    }
//  }

//  "ApplicationRoutes executing commands" should {
//    "request deployer to PREPARE cluster (POST /component/Preparation)" in {
//      val request = Post("/component/Preparation")
//      request ~> routes ~> check {
//        status should ===(StatusCodes.OK)
//        contentType should ===(ContentTypes.`application/json`)
//        entityAs[OperationResult].status should ===("Queued")
//      }
//    }
//    // TODO: how to check if the preparation was performed as expected?
//    "request deployer to FIX component (PATCH /component/Cassandra)" in {
//      val request = Patch("/component/Cassandra")
//      request ~> routes ~> check {
//        status should ===(StatusCodes.OK)
//        contentType should ===(ContentTypes.`application/json`)
//        entityAs[OperationResult].status should ===("Queued")
//      }
//    }
//    "request deployer to INSTALL component (POST /component/Cassandra)" in {
//      val request = Post("/component/Cassandra")
//      request ~> routes ~> check {
//        status should ===(StatusCodes.OK)
//        contentType should ===(ContentTypes.`application/json`)
//        entityAs[OperationResult].status should ===("Queued")
//      }
//    }
//    "request deployer to DEINSTALL component (DELETE /component/Cassandra)" in {
//      val request = Delete("/component/Cassandra")
//      request ~> routes ~> check {
//        status should ===(StatusCodes.OK)
//        contentType should ===(ContentTypes.`application/json`)
//        entityAs[OperationResult].status should ===("Queued")
//      }
//    }
//    "get application component (3) events (GET /component/Cassandra)" in {
//      val request = HttpRequest(uri = "/component/Cassandra")
//      eventually(timeout(Span(5, Seconds)), interval(Span(500, Millis))) {
//        request ~> routes ~> check {
//          status should ===(StatusCodes.OK)
//          contentType should ===(ContentTypes.`application/json`)
//          entityAs[ApplicationComponent].events.length shouldEqual 3
//        }
//      }
//    }
//    "request deployer to FIX missing component (DELETE /component/xpto123)" in {
//      val request = Patch("/component/xpto123")
//      request ~> routes ~> check {
//        status should ===(StatusCodes.BadRequest)
//        contentType should ===(ContentTypes.`application/json`)
//        entityAs[OperationResult].status should ===("Failure")
//      }
//    }
//  }
}
