package org.boardcrew.installer

import akka.actor.ActorRef
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.concurrent.Eventually._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{Matchers, WordSpec}
import spray.json._

class PreparationSpec extends WordSpec with Matchers with ScalaFutures with ScalatestRouteTest
  with ApplicationRoutes with JsonSupport {
  lazy val routes = userRoutes
  override val installation: ActorRef = system.actorOf(Installation.props, "InstallationActor")

  "ApplicationRoutes creating the topology for preparation only " should {
    "load configuration from file (POST /installation)" in {
      val configuration = scala.io.Source.fromResource("installation-preparation-only.json").mkString
      val installation: InstallationState = configuration.parseJson.convertTo[InstallationState]

      val configEntity = Marshal(installation).to[MessageEntity].futureValue
      val request = Post("/installation").withEntity(configEntity)

      request ~> routes ~> check {
        status should ===(StatusCodes.Created)
        contentType should ===(ContentTypes.`application/json`)
        entityAs[OperationResult].status should ===("Success")
      }
    }
    "return installation from file (GET /installation)" in {
      val request = HttpRequest(uri = "/installation")
      eventually(timeout(Span(10, Seconds)), interval(Span(1000, Millis))) {
        request ~> routes ~> check {
          status should ===(StatusCodes.OK)
          contentType should ===(ContentTypes.`application/json`)
          entityAs[InstallationState].name should ===("Installation preparation only")
          // Should contain the 4 application components defined in the configuration file
          entityAs[InstallationState].applicationComponents.length shouldEqual 1
        }
      }
    }
    "get application component (GET /component/Preparation)" in {
      val request = HttpRequest(uri = "/component/Preparation")
      eventually(timeout(Span(20, Seconds)), interval(Span(3, Seconds))) {
        request ~> routes ~> check {
          status should ===(StatusCodes.OK)
          contentType should ===(ContentTypes.`application/json`)
          entityAs[ApplicationComponent].name should ===("Preparation")
        }
      }
    }
    "load phases (PATCH /phases/Preparation)" in {
      val configuration = scala.io.Source.fromResource("Preparation-phases-test.json").mkString
      val phases: PhasesState = configuration.parseJson.convertTo[PhasesState]
      val configEntity = Marshal(phases).to[MessageEntity].futureValue
      val request = Patch("/phases/Preparation").withEntity(configEntity)
      eventually(timeout(Span(20, Seconds)), interval(Span(3, Seconds))) {
        request ~> routes ~> check {
          status should ===(StatusCodes.Created)
          contentType should ===(ContentTypes.`application/json`)
          entityAs[OperationResult].status should ===("Accepted")
          entityAs[OperationResult].phase should ===("Phases loaded")
        }
      }
    }
    //    "get application component (GET /phases/Preparation)" in {
    //      val request = HttpRequest(uri = "/phases/Preparation")
    //      eventually(timeout(Span(20, Seconds)), interval(Span(3, Seconds))) {
    //        request ~> routes ~> check {
    //          status should ===(StatusCodes.OK)
    //          contentType should ===(ContentTypes.`application/json`)
    //          entityAs[ApplicationComponent].name should ===("Preparation")
    //        }
    //      }
    //    }
  }
}

