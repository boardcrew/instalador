package org.boardcrew.installer

import java.time.Instant
import java.util.Base64

import akka.actor.{Actor, ActorLogging, Props}
import akka.util.Timeout
import akka.pattern.pipe
import com.typesafe.config.{Config, ConfigFactory}
import org.boardcrew.installer.Deployer.DeployerCommandOutputs
import org.boardcrew.installer.OSCommandExecutor.Execute

import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.concurrent.duration._
import scala.sys.process.ProcessLogger

object OSCommandExecutor {

  def props: Props = Props[OSCommandExecutor]

  final case class Execute( name: String,
                            operations: Seq[Operation])

}

class OSCommandExecutor extends Actor with ActorLogging {

  val config: Config = ConfigFactory.load()
  val localTimeout: FiniteDuration = config.getString("instalador.commandExecutionTimeout").toInt seconds

  def receive: Receive = {

    case Execute(name,operations) =>
      implicit val timeout: Timeout = Timeout(localTimeout)
      implicit val executionContext: ExecutionContextExecutor = context.dispatcher
      val maybeCommandOutputs: Future[Iterable[CommandOuput]] = {
        Future.traverse(operations) { operation =>
          log.info(s"OSCommandExecutor: executing: ${operation.name}: ${operation.description}!")

          val command: String = if (operation.command.startsWith("B64:")) {
            log.debug(s"OSCommandExecutor: Command is B64 encoded: ${operation.command}")
            val textCommand = try {
              Base64.getDecoder.decode(operation.command.replace("B64:", ""))
                .map(_.toChar)
                .mkString
            } catch {
              case ex: Exception =>
                log.error(s"OSCommandExecutor: B64 ERROR: ${ex.getMessage}!")
                "return 1"
            }
            log.debug(s"OSCommandExecutor: B64 decoded: $textCommand")
            textCommand
          } else {
            operation.command
          }
          log.debug(s"OSCommandExecutor: executing: $command")


          val (code: Int, output: Option[String],error: Option[String]) =
            try {
              executeCommad(command)
            } catch {
              case ex: Exception =>
                (99,Some(s"${ex.getLocalizedMessage}"))
            }

          Future(CommandOuput(Instant.now().toString, s"$code", operation.command, output, error))
        }
      }

      pipe(maybeCommandOutputs.map(result => DeployerCommandOutputs(name, CommandOuputs(result.asInstanceOf[Seq[CommandOuput]])))
        .recover {
          case ex: Exception =>
            log.error(s"OSCommandExecutor [$name]: ERROR: ${ex.getLocalizedMessage} for operations: $operations")
            val co = CommandOuput("1",
              Instant.now().toString,
              s"OSCommandExecutor raised error on complete of operations: $operations",
              None,
              Some(ex.toString))
            DeployerCommandOutputs("", CommandOuputs(Seq(co)))
        }
      ).to(sender)
  }

  def executeCommad(command: String): (Int,Option[String],Option[String]) = {
    val out = new StringBuilder
    val err = new StringBuilder
    var codeInt: Int = 0
    val logger = ProcessLogger(
      (o: String) => out.append(o),
      (e: String) => err.append(e))

    try {
      import sys.process._
      // Run everything as a bash shell!!!
      codeInt = Seq("/bin/bash","-c",command) ! logger
      log.debug(s"OSCommandExecutor.executeCommand.code($codeInt).STDOUT: $out, STDERR: $err")
      (codeInt,Some(out.mkString),Some(err.mkString))
    } catch {
      case ex: Exception =>
        log.error(s"OSCommandExecutor: ERROR: ${ex.getMessage}: $err!")
        (codeInt,Some(out.mkString),Some(s"${err.mkString}:-:-:$err"))
    }
  }
}

