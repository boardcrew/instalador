package org.boardcrew.installer

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.pattern.ask
import akka.util.Timeout
import com.typesafe.config.{Config, ConfigFactory}

import scala.concurrent.duration._
import org.boardcrew.installer.Deployer.FilledPhases
import org.boardcrew.installer.Initializer.{FillPhases, FillPhasesString}
import org.boardcrew.installer.Phases.GetPhases

import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.util.{Failure, Success}

object Initializer {

  def props: Props = Props[Initializer]

  final case class FillPhases(componentName: String,deployer: String, commandsURI: String, controlParameters: Seq[Parameter],deployerParameters: Seq[Parameter])
  final case class FillPhasesString(componentName: String, deployer: String, phases: PhasesState, controlParameters: Seq[Parameter], deployerParameters: Seq[Parameter])

}

class Initializer extends Actor with ActorLogging with JsonSupport with PhasesUtils {

  val config: Config = ConfigFactory.load()
  implicit val timeout: Timeout = Timeout(config.getString("instalador.getPhasesTimeout").toInt seconds)
  implicit val executionContext: ExecutionContextExecutor = context.dispatcher
  var filledPhases: Option[PhasesState] = None
  var phasesActor: Option[ActorRef] = None

  override def receive: Receive = {
    case FillPhases(componentName, deployer, _, controlParameters, deployerParameters) =>
      val localSender = sender
      log.debug(s"Initializer[$componentName]: Phases.GetPhases")
      val maybeComponentPhases: Future[Option[PhasesState]] = (context.actorSelection("/user/Phases") ? GetPhases(deployer)).mapTo[Option[PhasesState]]
      maybeComponentPhases.onComplete {
        case Success(Some(componentPhases)) =>
          log.debug(s"Initializer[$componentName]: $componentName componentPhases -> $componentPhases")
          filledPhases = fillPhases(Some(componentPhases), controlParameters, deployerParameters)
          log.debug(s"Initializer[$componentName]: $componentName filledPhases -> $filledPhases")
          localSender ! FilledPhases(filledPhases)
        case Success(None) =>
          log.warning(s"Initializer[$componentName]: no phases found for deployer $deployer!")
          localSender ! NoPhasesFound
        case Failure(ex) =>
          log.error(s"Initializer[$componentName]: $componentName phases failure: ${ex.getLocalizedMessage} ")
          localSender ! InitializerException(ex.getLocalizedMessage)
      }

    case FillPhasesString(componentName, _, phases, controlParameters, deployerParameters) =>
      filledPhases = fillPhases(Some(phases), controlParameters, deployerParameters)
      log.debug(s"Initializer[$componentName]: $componentName phases -> $filledPhases")
      sender ! FilledPhases(filledPhases)

    case _ =>
      log.warning(s"Initializer received invalid message from ${sender.toString()}!")
      sender ! InitializerException("Invalid message")
  }
}
