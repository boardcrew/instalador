package org.boardcrew.installer

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.stream.ActorMaterializer
import org.boardcrew.installer.K8SIntegration.{AssertAppCompState, GetPOD}
import skuber._
import skuber.json.format._

import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration._
import scala.util.{Failure, Success}

case class PodState(name: String,
                    status: String)

object K8SIntegration {
  def props: Props = Props[K8SIntegration]

  final case class GetPOD(namespace: String, podName: String)

  final case class AssertAppCompState()

}

class K8SIntegration extends Actor with ActorLogging with JsonSupport {

  implicit val system: ActorSystem = context.system
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executionContext: ExecutionContextExecutor = context.dispatcher

  var response: Option[PodState] = None

  override def preStart(): Unit = {
    context.system.scheduler.schedule(initialDelay = 5 seconds, interval = 5 seconds, receiver = self, message = AssertAppCompState)
  }

  override def receive: Receive = {

    case GetPOD(namespace, podName) =>
      log.debug(s"K8SIntegration.GetPOD: $namespace.$podName")
      val k8s = k8sInit
      k8s.getInNamespace[Pod](podName, namespace)
      val podRequest = k8s.get[Pod](podName)
      podRequest.onComplete {
        case Success(pod) =>
          response = Some(PodState(pod.name, pod.status.getOrElse("-").toString))
        case Failure(ex) =>
          log.error(s"K8SIntegration.GetPOD: error: ${ex.getLocalizedMessage}")
      }
      sender ! response


    case AssertAppCompState =>
      log.debug("Asserting the status of this component in the Kubernetes cluster")
      //TODO: Connect to the Kubernetes cluster
      //TODO: Based on the application component defintion define the K8S components supposed to be present in the cluster
      //TODO: (deployment, statefulset, configMap, secrets)
      //TODO: Get the version of the container images in the PODs of this application component
      //TODO: how to define the expected components of one deployment?

    case _ =>
      log.warning(s"K8SIntegration received invalid message from ${sender.toString()}!")
      sender ! None
  }
}