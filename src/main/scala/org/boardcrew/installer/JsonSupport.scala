package org.boardcrew.installer

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.{DefaultJsonProtocol, RootJsonFormat}

trait JsonSupport extends SprayJsonSupport {
  import DefaultJsonProtocol._
  implicit val commandOutputFormat: RootJsonFormat[CommandOuput] = jsonFormat5(CommandOuput)
  implicit val operationEventFormat: RootJsonFormat[OperationEvent] = jsonFormat7(OperationEvent)
  implicit val operationEventsFormat: RootJsonFormat[OperationEvents] = jsonFormat1(OperationEvents)
  implicit val operationResultFormat: RootJsonFormat[OperationResult] = jsonFormat7(OperationResult)
  implicit val configurationFormat: RootJsonFormat[Configuration] = jsonFormat1(Configuration)
  implicit val parameterFormat: RootJsonFormat[Parameter] = jsonFormat3(Parameter)
  implicit val metricFormat: RootJsonFormat[Metric] = jsonFormat3(Metric)
  implicit val metricsFormat: RootJsonFormat[Metrics] = jsonFormat1(Metrics)
  implicit val applicationComponentFormat: RootJsonFormat[ApplicationComponent] = jsonFormat8(ApplicationComponent)
  implicit val installationStateFormat: RootJsonFormat[InstallationState] = jsonFormat6(InstallationState)
  implicit val operationformat: RootJsonFormat[Operation] = jsonFormat3(Operation)
  implicit val phasesStateFormat: RootJsonFormat[PhasesState] = jsonFormat6(PhasesState)
  implicit val PhasesSeqFormat: RootJsonFormat[PhasesSeq] = jsonFormat1(PhasesSeq)
}
