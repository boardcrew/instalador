package org.boardcrew.installer

import akka.actor.{ActorRef, ActorSystem}
import akka.event.Logging
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.HttpMethods._
import akka.http.scaladsl.model.headers.{`Access-Control-Allow-Methods`, `Access-Control-Allow-Origin`,`Access-Control-Allow-Headers`}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.MethodDirectives.{get, post}
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import akka.pattern.ask
import akka.util.Timeout
import org.boardcrew.installer.Installation._
import scala.concurrent.Future
import scala.concurrent.duration._

trait ApplicationRoutes extends JsonSupport {
  implicit def system: ActorSystem

  lazy val log = Logging(system, classOf[ApplicationRoutes])

  implicit lazy val timeout: Timeout = Timeout(10 seconds)
  lazy val userRoutes: Route =
    pathPrefix("installation") {
      concat(
        pathEnd {
          concat(
            post {
              entity(as[InstallationState]) { configuration =>
                val operationResult: Future[OperationResult] =
                  (installation ? LoadConfiguration(configuration)).mapTo[OperationResult]
                onSuccess(operationResult) { operationResult =>
                  log.info("SERVER: Configuration loaded!")
                  complete(StatusCodes.Created,List(`Access-Control-Allow-Origin`.*), operationResult)
                }
              }
            },
            delete {
              val operationResult: Future[OperationResult] =
                (installation ? CleanConfiguration).mapTo[OperationResult]
              onSuccess(operationResult) { operationResult =>
                log.info("SERVER: Configuration URL cleaned!")
                complete(StatusCodes.Accepted,List(`Access-Control-Allow-Origin`.*), operationResult)
              }
            },
            patch {
              entity(as[String]) { confgurationURL =>
                val operationResult: Future[OperationResult] =
                  (installation ? LoadConfigurationURL(confgurationURL)).mapTo[OperationResult]
                onSuccess(operationResult) { operationResult =>
                  log.info("SERVER: Configuration URL loaded!")
                  complete(StatusCodes.Accepted,List(`Access-Control-Allow-Origin`.*), operationResult)
                }
              }
            },
            get {
              val maybeInstallation: Future[Option[InstallationState]] = (installation ? GetInstallation).mapTo[Option[InstallationState]]
              log.debug(s"SERVER: GetInstallation!")
              rejectEmptyResponse {
                log.debug(s"SERVER: GetInstallation response: $maybeInstallation")
                complete(StatusCodes.OK,List(`Access-Control-Allow-Origin`.*),maybeInstallation)
              }
            },
            options {
              complete(StatusCodes.OK,List(`Access-Control-Allow-Methods`(OPTIONS, POST, GET, PATCH),
                                           `Access-Control-Allow-Origin`.*,
                                           `Access-Control-Allow-Headers`("content-type")),"OK")
            }
          )
        }
      )
    } ~
    pathPrefix("component") {
      concat(
        path(Segment) { component =>
          concat(
            patch {
              val operationResult: Future[OperationResult] =
                (installation ? FixApplicationComponent(component)).mapTo[OperationResult]
              onSuccess(operationResult) { operationResult =>
                log.info(s"SERVER: FixApplicationComponent $component")
                complete((StatusCodes.Accepted, List(`Access-Control-Allow-Origin`.*),operationResult))
              }
            },
            put {
              val operationResult: Future[OperationResult] =
                (installation ? InitializeApplicationComponent(component)).mapTo[OperationResult]
              onSuccess(operationResult) { operationResult =>
                log.info(s"SERVER: InitializeApplicationComponent $component")
                complete((StatusCodes.Accepted, List(`Access-Control-Allow-Origin`.*),operationResult))
              }
            },
            post {
              val operationResult: Future[OperationResult] =
                (installation ? InstallApplicationComponent(component)).mapTo[OperationResult]
              onSuccess(operationResult) { operationResult =>
                log.info(s"SERVER: InstallApplicationComponent $component")
                complete((StatusCodes.Accepted, List(`Access-Control-Allow-Origin`.*),operationResult))
              }
            },
            delete {
              val operationResult: Future[OperationResult] =
                (installation ? UninstallApplicationComponent(component)).mapTo[OperationResult]
              onSuccess(operationResult) { operationResult =>
                log.info(s"SERVER: UninstallApplicationComponent $component")
                complete((StatusCodes.Accepted, List(`Access-Control-Allow-Origin`.*),operationResult))
              }
            },
            get {
              val maybeComponent: Future[Option[ApplicationComponent]] = (installation ? GetApplicationComponentState(component)).mapTo[Option[ApplicationComponent]]
              log.debug(s"SERVER: GetApplicationComponentState: $component")
              rejectEmptyResponse {
                complete(StatusCodes.OK,List(`Access-Control-Allow-Origin`.*),maybeComponent)
              }
            },
            options {
              complete(StatusCodes.OK,List(`Access-Control-Allow-Methods`(OPTIONS, POST, PUT, GET, DELETE, PATCH),
                                           `Access-Control-Allow-Origin`.*,
                                           `Access-Control-Allow-Headers`("content-type")),"OK")
            }
          )
        },
        path( Segment / "events") { component =>
          concat(
            get {
              val maybeComponent: Future[Option[OperationEvents]] = (installation ? GetApplicationComponentEvents(component)).mapTo[Option[OperationEvents]]
              log.debug(s"SERVER: GetApplicationComponentEvents: $component")
              rejectEmptyResponse {
                complete(StatusCodes.OK,List(`Access-Control-Allow-Origin`.*),maybeComponent)
              }
            },
            options {
              complete(StatusCodes.OK,List(`Access-Control-Allow-Methods`(OPTIONS, GET),
                                           `Access-Control-Allow-Origin`.*,
                                           `Access-Control-Allow-Headers`("content-type")),"OK")
            }
          )
        },
        path( Segment / "metrics") { component =>
          concat (
            get {
              val maybeComponent: Future[Option[Metrics]] = (installation ? GetApplicationComponentMetrics(component)).mapTo[Option[Metrics]]
              log.debug(s"SERVER: GetApplicationComponentMetrics: $component")
              rejectEmptyResponse {
                complete(StatusCodes.OK,List(`Access-Control-Allow-Origin`.*),maybeComponent)
              }
            },
            options {
              complete(StatusCodes.OK,List(`Access-Control-Allow-Methods`(OPTIONS, GET),
                                           `Access-Control-Allow-Origin`.*,
                                           `Access-Control-Allow-Headers`("content-type")),"OK")
            }
          )
        }
      )
    } ~
    pathPrefix("phases") {
      concat(
        path(Segment) { component =>
          log.debug(s"SERVER: Phases: $component")
          concat(
            get {
              val maybePhases: Future[Option[PhasesState]] = (installation ? GetApplicationComponentPhases(component)).mapTo[Option[PhasesState]]
              log.debug(s"SERVER: GetApplicationComponentPhases: $component")
              complete(StatusCodes.Accepted, maybePhases)
//              rejectEmptyResponse {
//                log.debug(s"SERVER: GetApplicationComponentPhases: maybePhases $maybePhases")
//                try {
//                  complete(maybePhases)
//                } catch {
//                  case ex: Exception =>
//                    log.debug(s"SERVER: GetApplicationComponentPhases exception: ${ex.getLocalizedMessage}")
//                    failWith(ex)
//                }
//              }
            },
            patch {
              entity(as[PhasesState]) { phases =>
                log.debug(s"SERVER: LoadPhasesApplicationComponent: $component")
                val operationResult: Future[OperationResult] =
                  (installation ? LoadPhasesApplicationComponent(component, phases)).mapTo[OperationResult]
                onSuccess(operationResult) { operationResult =>
                  log.info(s"SERVER: Component $component phases loaded!")
                  complete((StatusCodes.Created,List(`Access-Control-Allow-Origin`.*), operationResult))
                }
              }
            },
            options {
              complete(StatusCodes.OK,List(`Access-Control-Allow-Methods`(OPTIONS, GET, PATCH),
                                           `Access-Control-Allow-Origin`.*,
                                           `Access-Control-Allow-Headers`("content-type")),"OK")
            }
          )
        } ~
        post {
          entity(as[PhasesState]) { phases =>
            log.debug(s"SERVER: AddPhases: ${phases.name}")
            val operationResult: Future[OperationResult] =
              (installation ? AddInstallationPhases(phases)).mapTo[OperationResult]
            onSuccess(operationResult) { operationResult =>
              log.info(s"SERVER: Phases added!")
              complete((StatusCodes.Created,List(`Access-Control-Allow-Origin`.*), operationResult))
            }
          }
        } ~
        get {
          val maybePhasesSeq: Future[PhasesSeq] = (installation ? GetAllPhases).mapTo[PhasesSeq]
          log.debug(s"SERVER: GetAllPhases!")
          rejectEmptyResponse {
            log.debug(s"SERVER: GetAllPhases response: $maybePhasesSeq")
            complete(StatusCodes.OK,List(`Access-Control-Allow-Origin`.*),maybePhasesSeq)
          }
        } ~
        options {
          complete(StatusCodes.OK,List(`Access-Control-Allow-Methods`(OPTIONS, POST, GET),
                                       `Access-Control-Allow-Origin`.*,
                                       `Access-Control-Allow-Headers`("content-type")),"OK")
        }
      )
    } ~
    pathPrefix("healthcheck") {
      concat(
        pathEnd {
          concat(
            get {
              complete(StatusCodes.OK, "HEALTHY")
            }
          )
        }
      )
    }

  def installation: ActorRef
}
