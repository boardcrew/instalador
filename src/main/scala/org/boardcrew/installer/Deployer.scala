package org.boardcrew.installer

import java.time.Instant
import java.util.UUID.randomUUID

import akka.actor.{Actor, ActorLogging, ActorRef, InvalidActorNameException, PoisonPill, Props}
import akka.util.Timeout
import com.typesafe.config.ConfigFactory
import org.boardcrew.installer.Deployer._
import org.boardcrew.installer.Initializer.{FillPhases, FillPhasesString}
import org.boardcrew.installer.OSCommandExecutor.Execute
import org.boardcrew.installer.Publisher.PublishEvent

import scala.concurrent.duration._
import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

object Deployer {

  def props: Props = Props[Deployer]

  final case class GetState()

  final case class Initialize(component: ApplicationComponent, instConfigurationParams: Seq[Parameter], instDeployerParams: Seq[Parameter])

  final case class InitializeUnitilialized(component: ApplicationComponent, instConfigurationParams: Seq[Parameter],instDeployerParams: Seq[Parameter])

  final case class FilledPhases(phases: Option[PhasesState])

  final case class DeployerCommandOutputs(name: String, cos: CommandOuputs)

  final case class UpdateStatus()

  final case class Fix()

  final case class Install()

  final case class Uninstall()

  final case class ShowPlan()

  final case class PurgeEvents()

  final case class GetDeployerPhases()

  final case class LoadPhases(phases: PhasesState)

  final case class PropagateEvent(event: OperationEvent)

}


class Deployer extends Actor with ActorLogging with JsonSupport with PhasesUtils{
  import context._

  implicit val timeout: Timeout = Timeout(13 seconds)
  implicit val executionContext: ExecutionContext = context.dispatcher
//  val localTimeout = 5 minutes
  val deployerTypeParameter = "deployer"
  val deployerURIParameter = "deployerCommandsURI"
  var applicationComponent: Option[ApplicationComponent] = None
  var installationConfParameters: Option[Seq[Parameter]] = None
  var installationDeplParameters: Option[Seq[Parameter]] = None
  var filledPhases: Option[PhasesState] = None
  var componentDeployer: Option[String] = None
  var componentDeployerURI: Option[String] = None
  var events: Seq[OperationEvent] = Seq()
  var initializer: Option[ActorRef] = None
  var k8sasserter: Option[ActorRef] = None
  var publisher: Option[ActorRef] = None


  override def preStart(): Unit = {

    try {
      initializer = Some(context.actorOf(Initializer.props, s"${self.path.name}-Initializer"))
    }
    catch {
      case iane: InvalidActorNameException => log.warning(s"Deployer preStart initializer actor already exists: ${iane.message}!")
    }

//    try {
//      k8sasserter = Some(context.actorOf(K8SIntegration.props, s"${self.path.name}-K8S"))
//    }
//    catch {
//      case iane: InvalidActorNameException => log.warning(s"Deployer preStart k8sasserter actor already exists: ${iane.message}!")
//    }

    val config = ConfigFactory.load()
    if (config.getString("instalador.heartbeatEnabled").toBoolean) {

      try {
        context.actorSelection("/user/Publisher").resolveOne().onComplete {
          case Success(publisherActor) => publisher = Some(publisherActor)
          case Failure(ex) => log.warning(s"Deployer[publishEvent] failure selecting publisher actor: ${ex.getLocalizedMessage}")
        }
      } catch {
        case ex: Exception =>
          logger.error(s"publishEvent error: ${ex.getLocalizedMessage}")
      }
    } else {
      log.info("Heartbeat to Elasticsearch not enabled!")
    }

    log.debug("preStart: Becoming uninitialized")
    become(uninitialized)
  }

  override def receive: Receive = {
    case Initialize =>
      become(uninitialized)
  }

  def uninitialized: Receive = {

    case Initialize(component: ApplicationComponent, instConfigurationParams: Seq[Parameter],instDeployerParams: Seq[Parameter]) =>
      self ! InitializeUnitilialized(component,instConfigurationParams,instDeployerParams)

    case InitializeUnitilialized(component: ApplicationComponent, instConfigurationParams: Seq[Parameter],instDeployerParams: Seq[Parameter]) =>
//      applicationComponent = Some(component.copy(name = component.name.toLowerCase, state = "uninitialized"))
      applicationComponent = Some(component.copy(name = component.name.toLowerCase))
      installationConfParameters = Some(instConfigurationParams)
      installationDeplParameters = Some(instDeployerParams)

      getDeployerTypeAndURI()

      (componentDeployer.isDefined,componentDeployerURI.isDefined) match {
        case (true,true) =>

          context.actorSelection(s"${applicationComponent.get.name}-Initializer").resolveOne().onComplete {
            case Success(_) =>
              log.debug(s"Initializer ${applicationComponent.get.name}-Initializer present!")
            case Failure(_) =>
              log.debug(s"Creating ${applicationComponent.get.name}-Initializer!")
              try {
                initializer = Some(context.actorOf(Initializer.props, s"${self.path.name}-Initializer"))
              }
              catch {
                case iane: InvalidActorNameException => log.warning(s"Deployer InitializeUnitilialized initializer actor already exists: ${iane.message}!")
              }
          }

          val instConfParamToAdd: Seq[String] = installationConfParameters.get.map(_.name).diff(applicationComponent.get.controlParameters.map(_.name))
          val componentConfParams = applicationComponent.get.controlParameters.union(installationConfParameters.get.filter(d => instConfParamToAdd.contains(d.name)))
          log.debug(s"[${applicationComponent.get.name}] componentConfParams: $componentConfParams")

          val instDeplParamToAdd: Seq[String] = installationDeplParameters.get.map(_.name).diff(applicationComponent.get.deployerParameters.map(_.name))
          val componentDeployerParams = applicationComponent.get.deployerParameters.union(installationDeplParameters.get.filter(d => instDeplParamToAdd.contains(d.name)))
          log.debug(s"[${applicationComponent.get.name}] componentDeployerParams: $componentDeployerParams")

          initializer.get ! FillPhases(applicationComponent.get.name,componentDeployer.get,componentDeployerURI.get,componentConfParams,componentDeployerParams)
          self ! PropagateEvent(OperationEvent(Instant.now().toString, "Deployer" , "uninitialized", applicationComponent.get.id, applicationComponent.get.name, Seq(), s"[${applicationComponent.get.name}] initializing"))
          log.debug(s"## DEPLOYER CHANGING BEHAVIOR[${applicationComponent.get.name}]: Becoming initializing")
          become(initializing)
        case _ =>
          log.warning("Deployer or deployerURI is not informed, unable to start initialization!")
      }

    case InitializerException(errmsg: String) =>
      log.error(s"Deployer ${applicationComponent.get.name} initialier ERROR: $errmsg")

    case GetState => sender ! None

    case PropagateEvent(event) => events = publishEvent(event,events)

    case x: Any =>
      log.warning(s"Deployer[uninitialized] ${context.self.toString()} uninitialized, unknown message from ${sender.toString()}")
      sender ! OperationResult(
        Instant.now().toString,
        "initializing",
        "unknown message",
        applicationComponent.get.id,
        applicationComponent.get.name,
        None,
        None
      )
      self ! PropagateEvent(
        OperationEvent(
          Instant.now().toString,
          "Deployer" ,
          "uninitialized",
          applicationComponent.get.id,
          applicationComponent.get.name,
          Seq(CommandOuput(
            Instant.now().toString,
            "Warning",
            s"${x.getClass}",
            Option(s"unknown message from ${sender.toString()}"),
            None)),
          s"[${applicationComponent.get.name}] unknown message received")
        )
  }

  def initializing: Receive = {

    case FilledPhases(receivedFilledPhases: Option[PhasesState]) =>
      filledPhases = receivedFilledPhases
      log.debug(s"Deployer[${componentDeployer.get}]: ${applicationComponent.get.name} FilledPhases phases -> $filledPhases")
      self ! PropagateEvent(OperationEvent(Instant.now().toString, "Deployer" , "initializing", applicationComponent.get.id, applicationComponent.get.name, Seq(), s"[${applicationComponent.get.name}] FilledPhasesReceived"))
      log.debug(s"## DEPLOYER CHANGING BEHAVIOR[${applicationComponent.get.name}]: Becoming servicing")
      become(servicing)

    case Initialize(component: ApplicationComponent, instConfigurationParams: Seq[Parameter],instDeployerParams: Seq[Parameter]) =>
      log.warning(s"## DEPLOYER CHANGING BEHAVIOR[${applicationComponent.get.name}]: Becoming uninitialized while initializing!!!!!")
      become(uninitialized)
      self ! InitializeUnitilialized(component,instConfigurationParams,instDeployerParams)

    case GetState => sender ! None

    case NoPhasesFound =>
      log.warning(s"Deployer ${applicationComponent.get.name} no phases found by the initializer, becoming uninitialized!")
      become(uninitialized)

    case InitializerException(errmsg: String) =>
      log.error(s"Deployer ${applicationComponent.get.name} initialier ERROR: $errmsg")
      become(uninitialized)

    case PropagateEvent(event) => events = publishEvent(event,events)

    case x: Any =>
      log.warning(s"Deployer[initializing] ${applicationComponent.get.name} is initializing, unknown message from ${sender.toString()}!")
      sender ! OperationResult(
        Instant.now().toString,
        "initializing",
        "unknown message",
        applicationComponent.get.id,
        applicationComponent.get.name,
        None,
        None
      )
      self ! PropagateEvent(
        OperationEvent(
          Instant.now().toString,
          "Deployer" ,
          "initializing",
          applicationComponent.get.id,
          applicationComponent.get.name,
          Seq(CommandOuput(
            Instant.now().toString,
            "Warning",
            s"${x.getClass}",
            Option(s"unknown message from ${sender.toString()}"),
            None)),
          s"[${applicationComponent.get.name}] unknown message received")
      )
  }

  def changingPhase: Receive = {
    case GetState =>
      try {
        //log.debug(s"DEPLOYER[${componentDeployer.get}]: GetState ${applicationComponent.get.name} !")
//        applicationComponent = Some(applicationComponent.get.copy(timestamp = Instant.now().toString, state = "changingPhase", events = events))
        applicationComponent = Some(applicationComponent.get.copy(timestamp = Instant.now().toString, events = events))
        sender ! applicationComponent
      } catch {
        case ex: Exception =>
          log.error(s"DEPLOYER[${componentDeployer.get}]: GetState ${ex.getLocalizedMessage} !")
          None
      }

    case DeployerCommandOutputs(name,cos) =>
      log.debug(s"DEPLOYER[${componentDeployer.get}]: $name result length: ${cos.commadOutputs.length}!")
      log.debug(s"DEPLOYER[${componentDeployer.get}]: Result output: ${cos.commadOutputs.map { output => output.output }}!")
      sender ! PoisonPill
      self ! PropagateEvent(OperationEvent(Instant.now().toString, "Deployer", "changingPhase", applicationComponent.get.id, applicationComponent.get.name, cos.commadOutputs, s"[${applicationComponent.get.name}] DeployerCommandOutputsReceived"))
      log.debug(s"## DEPLOYER CHANGING BEHAVIOR[${applicationComponent.get.name}]: Becoming servicing")
      become(servicing)

    case PropagateEvent(event) => events = publishEvent(event,events)

    case x: Any =>
      log.warning(s"Deployer[changingPhase] ${applicationComponent.get.name},unknown message from ${sender.toString()}")
      sender ! OperationResult(
        Instant.now().toString,
        "ChangingPhase",
        "unknown message",
        applicationComponent.get.id,
        applicationComponent.get.name,
        None,
        None
      )
      self ! PropagateEvent(
        OperationEvent(
          Instant.now().toString,
          "Deployer" ,
          "changingPhase",
          applicationComponent.get.id,
          applicationComponent.get.name,
          Seq(CommandOuput(
            Instant.now().toString,
            "Warning",
            s"${x.getClass}",
            Option(s"unknown message from ${sender.toString()}"),
            None)),
          s"[${applicationComponent.get.name}] unknown message received")
      )
  }

  def servicing: Receive = {

    case Initialize(component: ApplicationComponent, instConfigurationParams: Seq[Parameter],instDeployerParams: Seq[Parameter]) =>
      log.debug(s"## DEPLOYER CHANGING BEHAVIOR[${applicationComponent.get.name}]: Becoming uninitialized")
      become(uninitialized)
      self ! InitializeUnitilialized(component,instConfigurationParams,instDeployerParams)

    case FilledPhases(_: Option[PhasesState]) => log.warning(s"Deployer[${componentDeployer.get}]: ${applicationComponent.get.name} Late FilledPhases received!")

    case PurgeEvents =>
      events = Seq()
      self ! PropagateEvent(OperationEvent(Instant.now().toString, "Deployer", "servicing", applicationComponent.get.id, applicationComponent.get.name, Seq(), s"[${applicationComponent.get.name}] Eventspurged"))
      log.warning(s"Deployer[${componentDeployer.get}]: ${applicationComponent.get.name} PurgeEvents")

    case LoadPhases(phases) =>
      log.debug(s"Deployer[${componentDeployer.get}]: ${applicationComponent.get.name} LoadPhases phases -> $phases")
      log.debug(s"## DEPLOYER CHANGING BEHAVIOR[${applicationComponent.get.name}]: Becoming initializing")
      become(initializing)
      initializer.get ! FillPhasesString(applicationComponent.get.name,componentDeployer.get,phases,applicationComponent.get.controlParameters,applicationComponent.get.deployerParameters)
      self ! PropagateEvent(OperationEvent(Instant.now().toString, "Deployer", "servicing", applicationComponent.get.id, applicationComponent.get.name, Seq(), s"[${applicationComponent.get.name}] phases loaded, initializing"))

    case GetDeployerPhases =>
      try {
        log.debug(s"DEPLOYER[${componentDeployer.get}]: GetPhases ${applicationComponent.get.name} !")
        sender ! filledPhases
      } catch {
        case ex: Exception =>
          log.error(s"DEPLOYER[${componentDeployer.get}]: GetPhases ${ex.getLocalizedMessage} !")
          None
      }

//    case FilledPhases(receivedFilledPhases: Option[Phases]) =>
//      filledPhases = receivedFilledPhases
//      log.debug(s"Deployer[${componentDeployer.get}]: ${applicationComponent.get.name} FilledPhases phases -> $filledPhases")
//      self ! PropagateEvent(OperationEvent(Instant.now().toString,
//        "Servicing",
//        "FilledPhases",
//        applicationComponent.get.id,
//        applicationComponent.get.name,
//        Seq()
//      ))

    case GetState =>
      try {

        //TODO: K8S
//        log.debug(s"DEPLOYER[${componentDeployer.get}]: GetState ${applicationComponent.get.name} !")
//              val maybePod: Future[Option[PodState]] = (k8sintegration ? GetPOD("default","busybox")).mapTo[Option[PodState]]
//              maybePod.onComplete {
//                case Success(Some(pod)) => {
//                  log.info(s"INSTALLATION:POD found: ${pod.name}/${pod.status}")
//                }
//                case Success(None) => {
//                  log.info(s"INSTALLATION:POD NOT found")
//                }
//                case Failure(ex) =>
//                  log.error(s"INSTALLATION: GetState k8sintegrations failure: ${ex.getLocalizedMessage} ")
//              }

//        applicationComponent = Some(applicationComponent.get.copy(timestamp = Instant.now().toString, state = "servicing", events = events))
        applicationComponent = Some(applicationComponent.get.copy(timestamp = Instant.now().toString, events = events))
        sender ! applicationComponent
      } catch {
        case ex: Exception =>
          log.error(s"DEPLOYER[${componentDeployer.get}]: GetState ${ex.getLocalizedMessage} !")
          None
      }

    case PropagateEvent(event) => events = publishEvent(event,events)

    case Fix =>
      log.debug(s"DEPLOYER[${componentDeployer.get}]: FixApplicationComponent ${applicationComponent.get.name}!")
      val executorID: String = s"${applicationComponent.get.name}-${randomUUID().toString}"
      val executor = context.actorOf(OSCommandExecutor.props, executorID)
      val commands: Seq[Operation] = filledPhases match {
        case Some(phase) => phase.upgrade
        case None => Seq()
      }
      executor ! Execute("Fix",commands)
      self ! PropagateEvent(OperationEvent(Instant.now().toString, "Deployer", "servicing", applicationComponent.get.id, applicationComponent.get.name, Seq(), s"[${applicationComponent.get.name}] fix command received"))
      log.debug(s"## DEPLOYER CHANGING BEHAVIOR[${applicationComponent.get.name}]: By fix becoming changingPhase")
      become(changingPhase)

    case Install =>
      log.debug(s"DEPLOYER[${componentDeployer.get}]: InstallApplicationComponent ${applicationComponent.get.name}!")
      val executorID = s"${applicationComponent.get.name}-${randomUUID().toString}"
      val executor = context.actorOf(OSCommandExecutor.props, executorID)
      val commands: Seq[Operation] = filledPhases match {
        case Some(phase) => phase.install
        case None => Seq()
      }
      executor ! Execute("Install",commands)
      self ! PropagateEvent(OperationEvent(Instant.now().toString, "Deployer", "servicing", applicationComponent.get.id, applicationComponent.get.name, Seq(), s"[${applicationComponent.get.name}] install command received"))
      log.debug(s"## DEPLOYER CHANGING BEHAVIOR[${applicationComponent.get.name}]: By install becoming changingPhase")
      become(changingPhase)

    case Uninstall =>
      log.debug(s"DEPLOYER[${componentDeployer.get}]: UninstallApplicationComponent ${applicationComponent.get.name}!")
      val executorID = s"${applicationComponent.get.name}-${randomUUID().toString}"
      val executor = context.actorOf(OSCommandExecutor.props, executorID)
      val commands: Seq[Operation] = filledPhases match {
        case Some(phase) => phase.uninstall
        case None => Seq()
      }
      executor ! Execute("Uninstall",commands)
      self ! PropagateEvent(OperationEvent(Instant.now().toString, "Deployer", "servicing", applicationComponent.get.id, applicationComponent.get.name, Seq(), s"[${applicationComponent.get.name}] uninstall command received"))
      log.debug(s"## DEPLOYER CHANGING BEHAVIOR[${applicationComponent.get.name}]: By uninstall becoming changingPhase")
      become(changingPhase)

    case DeployerCommandOutputs(name,cos) =>
      log.debug(s"DEPLOYER[${componentDeployer.get}]: $name result length: ${cos.commadOutputs.length}!")
      log.debug(s"DEPLOYER[${componentDeployer.get}]: Result output: ${cos.commadOutputs.map { output => output.output }}!")
      sender ! PoisonPill
      self ! PropagateEvent(OperationEvent(Instant.now().toString, "Instalador", name, applicationComponent.get.id, applicationComponent.get.name, cos.commadOutputs, s"[${applicationComponent.get.name}] DeployerCommandOutputsReceived"))

    case x: Any =>
      log.warning(s"Deployer[servicing] ${applicationComponent.get.name} received invalid message from ${sender.toString()}, message not accepted!")
      sender ! OperationResult(
        Instant.now().toString,
        "servicing",
        "unknown message",
        applicationComponent.get.id,
        applicationComponent.get.name,
        None,
        None
      )
      self ! PropagateEvent(
        OperationEvent(
          Instant.now().toString,
          "Deployer" ,
          "servicing",
          applicationComponent.get.id,
          applicationComponent.get.name,
          Seq(CommandOuput(
            Instant.now().toString,
            "Warning",
            s"${x.getClass}",
            Option(s"unknown message from ${sender.toString()}"),
            None)),
          s"[${applicationComponent.get.name}] unknown message received")
      )
  }

  def getDeployerTypeAndURI() {
    log.debug(s"DEPLOYER PARAMETERS: ${applicationComponent.get.controlParameters}}")
    componentDeployer = try {
      Some(applicationComponent.get.controlParameters.filter(p => p.name == deployerTypeParameter).head.value)
    } catch {
      case ex: Exception =>
        logger.error(s"getDeployerTypeAndURI.componentDeployer: error: ${ex.getLocalizedMessage}")
        None
    }
    componentDeployerURI = try {
      Some(applicationComponent.get.controlParameters.filter(p => p.name == deployerURIParameter).head.value)
    } catch {
      case ex: Exception =>
        logger.error(s"getDeployerTypeAndURI.componentDeployerURI: error: ${ex.getLocalizedMessage}")
        None
    }
  }

  def publishEvent(event: OperationEvent,events: Seq[OperationEvent]): Seq[OperationEvent] = {

    val config = ConfigFactory.load()
    if (config.getString("instalador.heartbeatEnabled").toBoolean) {
      publisher match {
        case Some(publisherActor) => publisherActor ! PublishEvent(event)
        case None => logger.error(s"publishEvent: publisher Actor == None !!!")
      }
    } else {
      log.info("Heartbeat to Elasticsearch not enabled!")
    }
    events :+ event

  }

}