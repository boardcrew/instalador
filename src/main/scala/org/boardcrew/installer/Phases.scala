package org.boardcrew.installer

import org.fusesource.scalate.{TemplateEngine, TemplateSource}
import spray.json._
import org.slf4j.{Logger, LoggerFactory}
import java.util.{Base64, UUID}

import akka.actor.{Actor, ActorLogging, Props}
import org.boardcrew.installer.Deployer.GetState
import org.boardcrew.installer.Phases.{AddPhases, GetPhases, GetPhasesSeq}

object Phases {
  def props: Props = Props[Phases]
  final case class AddPhasesURI(commandsURI: String)
  final case class AddPhases(phases: PhasesState)
  final case class GetPhases(phasesName: String)
  final case class GetPhasesSeq()
}

class Phases extends Actor with ActorLogging with JsonSupport with PhasesUtils {

  var phasesMap: Map[String,PhasesState] = Map()

  override def receive: Receive = {

    case AddPhases(phases) =>
      log.debug(s"Phases.AddPhases: ${phases.name}")
      phasesMap = phasesMap + (phases.name -> phases)
      log.debug(s"Phases.AddPhases: phasesMap ${phasesMap.mkString}")
    //filledPhases = fillPhases(readCommandsResources(commandsURI), controlParameters, deployerParameters)

    case GetPhases(phasesName) =>
      log.debug(s"Phases.GetPhases: phasesName $phasesName")
      log.debug(s"Phases.GetPhases: phasesMap ${phasesMap.mkString}")
      sender ! phasesMap.get(phasesName)

    case GetPhasesSeq =>
      log.debug(s"Phases.GetAllPhases!")
      sender ! PhasesSeq(phasesMap.values.toSeq)

    case GetState => sender ! None

    case _ =>
      log.warning(s"Phases received invalid message from ${sender.toString()}!")
  }
}

final case class PhasesSeq (allPhases: Seq[PhasesState])

final case class PhasesState(
                              name: String,
                              version: String,
                              install: Seq[Operation],
                              uninstall: Seq[Operation],
                              upgrade: Seq[Operation],
                              downgrade: Seq[Operation]
                            )

final case class Operation(
                            name: String,
                            description: Option[String],
                            command: String
                          )

trait PhasesUtils extends JsonSupport{
  val logger: Logger = LoggerFactory.getLogger(classOf[PhasesUtils])

  def parseCommandsString(phasesJSON: String): Option[PhasesState] = {
    try {
      Some(phasesJSON.parseJson.convertTo[PhasesState])
    } catch {
      case ex: Exception =>
        logger.error(s"parseCommandsString: error: ${ex.getLocalizedMessage}")
        None
    }
  }

  def readCommandsResources(resourcename: String): Option[PhasesState] = {
    try {
      val phasesJSON = scala.io.Source.fromResource(resourcename).mkString
      parseCommandsString(phasesJSON)
    } catch {
      case ex: Exception =>
        logger.error(s"readCommandsResources: error: ${ex.getLocalizedMessage}")
        None
    }
  }

  def fillPhases(phases: Option[PhasesState], controlParameters: Seq[Parameter], deployerParameters: Seq[Parameter]) : Option[PhasesState] = phases match {
    case Some(phasesConfig)  =>

      val controlParametersMap = Map(controlParameters map { a => a.name -> a.value }: _*)
      logger.trace(s"fillPhases: controlParametersMap: $controlParametersMap")
      val deployerParameterPrefix = controlParameters.filter(p => p.name == "deployerParameterPrefix").head.value
      val deployerParametersFlatten: String = deployerParameters.map { a => s" $deployerParameterPrefix ${a.name}=${a.value} " }.mkString(" ")
      logger.trace(s"fillPhases: deployerParametersMap: $deployerParametersFlatten")

      val parametersMapComplete = controlParametersMap + ("DEPLOYER_PARAMETERS" -> deployerParametersFlatten)
      logger.trace(s"fillPhases: parametersMapComplete: $parametersMapComplete")

      val filledPhases = phasesConfig.copy(
        install = renderMustache(phasesConfig.install,parametersMapComplete),
        uninstall = renderMustache(phasesConfig.uninstall,parametersMapComplete),
        upgrade = renderMustache(phasesConfig.upgrade,parametersMapComplete),
        downgrade = renderMustache(phasesConfig.downgrade,parametersMapComplete)
      )
      Some(filledPhases)
    case _  => None
  }

  def renderMustache(phase: Seq[Operation],parametersMapComplete: Map[String,String] ): Seq[Operation] = {
    logger.debug(s"renderMustache: ${phase.length} commands to process")
    val engine = new TemplateEngine
    val result: Seq[Operation] = phase.map { operation =>
      logger.trace("######################### MUSTACHE ############################")
      logger.trace(s"renderMustache: INPUT command: ${operation.command}")
      try {
        if (operation.command.startsWith("B64:")) {
          logger.debug(s"renderMustache: Command is B64 encoded: ${operation.command}")
          val textCommand = try {
            val commanddecoded = Base64.getDecoder.decode(operation.command.replace("B64:", ""))
              .map(_.toChar)
              .mkString
            val result = engine.layout(TemplateSource.fromText(s"${UUID.randomUUID()}.mustache", commanddecoded), parametersMapComplete)
            logger.trace(s"renderMustache: OUTPUT command: $result")

            val resultEncoded = s"B64:${Base64.getEncoder.encodeToString(result.getBytes("UTF-8"))}"
            Operation(operation.name, operation.description, resultEncoded)
          } catch {
            case ex: Exception =>
              logger.error(s"renderMustache: B64 ERROR: ${ex.getMessage}!")
              Operation(operation.name, operation.description, operation.command)
          }
          logger.debug(s"renderMustache: B64 decoded: $textCommand")
          textCommand
        } else {
          val result = engine.layout(TemplateSource.fromText(s"${UUID.randomUUID()}.mustache", operation.command), parametersMapComplete)
          logger.trace(s"renderMustache: OUTPUT command: $result")
          Operation(operation.name, operation.description, result)
        }
      } catch {
        case ex: Exception =>
          // TODO: org.fusesource.scalate.util.ResourceNotFoundException: Could not load resource:
          logger.error(s"renderMustache: RETURNING ORIGINAL COMMAND due to error: $ex")
          logger.trace(s"renderMustache: OUTPUT command: ${operation.command}")
          // TODO: Whaaaat??????????????????
          Operation(operation.name,operation.description,operation.command)
      }
    }
    logger.debug(s"renderMustache: RESULT: $result")
    result
  }

}
