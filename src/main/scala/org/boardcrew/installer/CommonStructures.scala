package org.boardcrew.installer

final case class Configuration(body: String)

final case class Metric(name: String, description: String, value: String)

final case class PlanItem(command: String, description: String)

final case class Plan(items: Seq[PlanItem])

final case class Metrics(metrics: Seq[Metric])

final case class NoPhasesFound()

final case class InitializerException(errmsg: String)

final case class Parameter(name: String, description: Option[String], value: String)

final case class ComponentEvent(timestamp: String, message: String)

final case class AppComponentConformity(id: String, name: String, conform: Boolean, cause: String)

final case class CommandOuput(timestamp: String,
                              status: String,
                              command: String,
                              output: Option[String],
                              error: Option[String])

final case class CommandOuputs(commadOutputs: Seq[CommandOuput])

final case class OperationEvents(operationEvents: Seq[OperationEvent])

final case class OperationEvent(timestamp: String,
                                installation: String,
                                phase: String,
                                componentID: String,
                                componentName: String,
                                commandsOutputs: Seq[CommandOuput],
                                status: String)

final case class OperationResult(timestamp: String,
                                 status: String,
                                 phase: String,
                                 componentID: String,
                                 componentName: String,
                                 eventID: Option[String],
                                 observation: Option[String])

final case class ApplicationComponent(timestamp: String,
                                      id: String,
                                      name: String,
                                      installed: Boolean,
                                      //state: String,
                                      controlParameters: List[Parameter],
                                      deployerParameters: Seq[Parameter],
                                      metrics: Seq[Metric],
                                      events: Seq[OperationEvent])

final case class InstallationState(name: String,
                                   version: String,
                                   compliant: Boolean,
                                   controlParameters: Seq[Parameter],
                                   deployerParameters: Seq[Parameter],
                                   applicationComponents: Seq[ApplicationComponent])
