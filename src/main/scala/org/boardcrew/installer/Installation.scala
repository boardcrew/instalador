package org.boardcrew.installer

import java.time.Instant
import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, InvalidActorNameException, Props}
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.pattern.ask
import akka.stream.ActorMaterializer
import akka.util.Timeout
import com.typesafe.config.ConfigFactory
import org.boardcrew.installer.Deployer._
import org.boardcrew.installer.Installation._
import org.boardcrew.installer.Phases.GetPhasesSeq
import org.boardcrew.installer.Publisher.PublishEvent
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContextExecutor, Future}
import scala.util.{Failure, Success}

object Installation {

  def props: Props = Props[Installation]

  final case class LoadURL()

  final case class LoadPOSTBody()

  final case class AskStatusUpdate()

  final case class UpdateConfiguration(instalacao: InstallationState)

  final case class LoadConfiguration(instalacao: InstallationState)

  final case class CleanConfiguration()

  final case class LoadConfigurationURL(configurationURL: String)

  final case class AddInstallationPhases(phases: PhasesState)

  final case class GetInstallation()

  final case class InitializeDeployers()

  final case class GetComplianceReport()

  final case class GetParameters()

  final case class GetAllPhases()

  final case class GetApplicationComponentState(component: String)

  final case class GetApplicationComponentEvents(component: String)

  final case class GetApplicationComponentMetrics(component: String)

  final case class LoadPhasesApplicationComponent(component: String, phases: PhasesState)

  final case class InitializeApplicationComponent(component: String)

  final case class GetApplicationComponentPhases(component: String)

  final case class InstallApplicationComponent(component: String)

  final case class UninstallApplicationComponent(component: String)

  final case class FixApplicationComponent(component: String)

  final case class ShowApplicationComponentPlan(component: String)

  final case class GetApplicationComponentParameters(component: String)

}

class Installation extends Actor with ActorLogging with JsonSupport {

  implicit val timeout: Timeout = Timeout(5 seconds)
  implicit val executionContext: ExecutionContextExecutor = context.dispatcher
  implicit val system: ActorSystem = context.system
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  //default value for minimum interval for loading configuration
  var configMinIntervalSeconds = 30
  var lastConfigLoadTime: Instant = Instant.now()
  val localTimeout: FiniteDuration = 8 seconds
  var phases: Option[ActorRef] = None
  var publisher: Option[ActorRef] = None
  var installationState: Option[InstallationState] = None

  override def preStart(): Unit = {
    context.system.scheduler.schedule(60 seconds, 5 seconds, self, AskStatusUpdate)

    //default value for minimum interval for loading configuration
    val config = ConfigFactory.load()
    configMinIntervalSeconds = config.getString("instalador.configMinIntervalSeconds").toInt

    try {
      log.info("INSTALLATION: preStart creating phases actor")
      phases = Some(context.system.actorOf(Phases.props, "Phases"))
    }
    catch {
      case iane: InvalidActorNameException => log.warning(s"INSTALLATION: preStart phases actor already exists: ${iane.message}!")
    }

    try {
      log.info("INSTALLATION: preStart creating publisher actor")
      publisher = Some(context.system.actorOf(Publisher.props, "Publisher"))
      publisher.get ! PublishEvent(OperationEvent(
        Instant.now().toString,
        "Instalador",
        "Startup",
        "",
        "Installation",
        Seq(),
        "PublisherStarted"))
    }
    catch {
      case iane: InvalidActorNameException => log.warning(s"INSTALLATION: preStart publisher actor already exists: ${iane.message}!")
    }
//    context.actorSelection("phases").resolveOne().onComplete {
//      case Failure(ex) => {
//        log.info(s"INSTALLATION: preStart creating new actor for: phases")
//        phases = Some(context.system.actorOf(Phases.props, "phases"))
//      }
//    }
  }

  def receive: Receive = {

    case AddInstallationPhases(newPhases) =>
      //      context.actorSelection("phases").resolveOne().onComplete {
      //        case Failure(ex) => {
      //          log.info(s"INSTALLATION: AddInstallationPhases creating new actor for phases due to error: $ex")
      //          try {
      //            val phasesActor = context.system.actorOf(Phases.props, "phases")
      //            phases = Some(phasesActor)
      //          }
      //          catch {
      //            case iane: InvalidActorNameException => log.warning(s"INSTALLATION: AddInstallationPhases phases actor already exists: ${iane.message}!")
      //          }
      //        }
      //      }
      val localSender = sender
      phases match {
        case Some(actor) =>
          actor ! org.boardcrew.installer.Phases.AddPhases(newPhases)
          localSender ! OperationResult(
            Instant.now().toString,
            "Success",
            s"AddPhases for deployer: ${newPhases.name}",
            "",
            "",
            None,
            None)
        case None =>
          try {
            log.info("INSTALLATION: AddInstallationPhases creating phases actor")
            phases = Some(context.system.actorOf(Phases.props, "Phases"))
          }
          catch {
            case iane: InvalidActorNameException => log.warning(s"INSTALLATION: AddInstallationPhases phases actor already exists: ${iane.message}!")
          }
          phases.get ! org.boardcrew.installer.Phases.AddPhases(newPhases)
          localSender ! OperationResult(
            Instant.now().toString,
            "Success",
            s"AddPhases for deployer: ${newPhases.name}",
            "",
            "",
            None,
            None)

          //          localSender ! OperationResult(
          //            Instant.now().toString,
          //            "Failure",
          //            "No phases created (actor=None)",
          //            "",
          //            "",
          //            None,
          //            None)
      }

    case GetApplicationComponentPhases(component) =>
      log.debug(s"INSTALLATION: GetApplicationComponentPhases lookup: ${component.toLowerCase}")
      val localComponent = component.toLowerCase
      val localSender = sender
      context.actorSelection(localComponent).resolveOne().onComplete {
        case Success(actor) =>
          val maybeResult = actor ? GetDeployerPhases
          val result = Await.result(maybeResult, localTimeout)
          log.debug(s"INSTALLATION: GetApplicationComponentPhases result from component: $result")
          val response = result match {
            case Some(comp) => Some(comp)
            case _ => None
          }
          log.debug(s"INSTALLATION: GetApplicationComponentPhases response: $response")
          localSender ! (response, self)
        //              sender() ! (actor ? GetPhases)

        case Failure(ex) =>
          log.error(s"INSTALLATION: GetApplicationComponentPhases: cannot find deployer with name $localComponent: ${ex.getLocalizedMessage}")
          localSender ! None
      }

    case LoadPhasesApplicationComponent(component, phasesDefinition) =>
      implicit val timeout: Timeout = Timeout(localTimeout)
      val localComponent = component.toLowerCase
      val localSender = sender
      context.actorSelection(localComponent).resolveOne().onComplete {
        case Success(deployerRef) =>
          log.info(s"INSTALLATION: [$localComponent] LoadPhasesApplicationComponent")
          deployerRef ! LoadPhases(phasesDefinition)
          val os = OperationResult(
            Instant.now().toString,
            "Delivered",
            s"message to $localComponent sent",
            "",
            "",
            None,
            None
          )
          localSender ! os
        case Failure(_) =>
          log.warning(s"INSTALLATION: [$localComponent] LoadPhasesApplicationComponent: component not found!")
          localSender ! OperationResult(
            Instant.now().toString,
            "NotFound",
            s"LoadPhasesApplicationComponent: $localComponent not found!",
            "",
            "",
            None,
            None
          )
      }

    case FixApplicationComponent(component) =>
      implicit val timeout: Timeout = Timeout(localTimeout)
      val localComponent = component.toLowerCase
      val localSender = sender
      context.actorSelection(localComponent).resolveOne().onComplete {
        case Success(deployerRef) =>
          log.info(s"INSTALLATION: [$localComponent] FixApplicationComponent")
          deployerRef ! Fix
          val os = OperationResult(
            Instant.now().toString,
            "Delivered",
            s"message Fix sent to $localComponent",
            "",
            "",
            None,
            None
          )
          localSender ! os
        case Failure(_) =>
          log.warning(s"INSTALLATION: [$localComponent] FixApplicationComponent: component not found!")
          localSender ! OperationResult(
            Instant.now().toString,
            "NotFound",
            s"FixApplicationComponent: $localComponent not found!",
            "",
            "",
            None,
            None
          )
      }

    case InstallApplicationComponent(component) =>
      implicit val timeout: Timeout = Timeout(localTimeout)
      val localComponent: String = component.toLowerCase
      val localSender = sender
      context.actorSelection(localComponent).resolveOne().onComplete {
        case Success(deployerRef) =>
          log.info(s"INSTALLATION: [$localComponent] InstallApplicationComponent")
          deployerRef ! Install
          val os = OperationResult(
            Instant.now().toString,
            "Delivered",
            s"message Install sent to $localComponent",
            "",
            "",
            None,
            None
          )
          localSender ! os
        case Failure(_) =>
          log.warning(s"INSTALLATION: [$localComponent] InstallApplicationComponent: component not found!")
          localSender ! OperationResult(
            Instant.now().toString,
            "NotFound",
            s"InstallApplicationComponent: $localComponent not found!",
            "",
            "",
            None,
            None
          )
      }

    case UninstallApplicationComponent(component) =>
      implicit val timeout: Timeout = Timeout(localTimeout)
      val localComponent = component.toLowerCase
      val localSender = sender
      context.actorSelection(localComponent).resolveOne().onComplete {
        case Success(deployerRef) =>
          log.info(s"INSTALLATION: [$localComponent] DeinstallApplicationComponent")
          deployerRef ! Uninstall
          val os = OperationResult(
            Instant.now().toString,
            "Delivered",
            s"message Uninstall sent to $localComponent",
            "",
            "",
            None,
            None
          )
          localSender ! os
        case Failure(_) =>
          log.warning(s"INSTALLATION: [$localComponent] UninstallApplicationComponent: component not found!")
          localSender ! OperationResult(
            Instant.now().toString,
            "NotFound",
            s"DeinstallApplicationComponent: $localComponent not found!",
            "",
            "",
            None,
            None
          )
      }

    case CleanConfiguration() =>
      installationState = None
      OperationResult(
        Instant.now().toString,
        "Clean",
        s"configuration clean",
        "",
        "",
        None,
        None
      )

    case LoadConfiguration(config) =>
      log.debug(s"INSTALLATION: Configuration received!")
      if (Instant.now().minusSeconds(configMinIntervalSeconds).isAfter(lastConfigLoadTime)) {
        lastConfigLoadTime = Instant.now()
        //TODO: merge into the current configuration
        //          val a = Generic[InstallationState]
        //          val b = Generic[InstallationState]
        //          val c = Generic[InstallationState].from(a.to(installationState) ++ b.to(config))

        installationState = Some(config)

        sender() ! OperationResult(
          Instant.now().toString,
          "Success",
          "LoadConfiguration",
          "",
          "",
          None,
          None)
        self ! InitializeDeployers
      } else {
        sender() ! OperationResult(
          Instant.now().toString,
          "Failure",
          "LoadConfiguration too early!",
          "",
          "",
          None,
          None)
      }

    case LoadConfigurationURL(configurationURL) =>
      if (Instant.now().minusSeconds(configMinIntervalSeconds).isAfter(lastConfigLoadTime)) {
        lastConfigLoadTime = Instant.now()
        import akka.http.scaladsl.Http
        import akka.http.scaladsl.model._
        val s = sender
        //TODO: add timeout of 5 seconds
        val maybeResponse: Future[HttpResponse] = Http().singleRequest(HttpRequest(uri = configurationURL))
        maybeResponse.onComplete {
          case Success(res) =>
            val maybeConfig: Future[InstallationState] = Unmarshal(res).to[InstallationState]
            maybeConfig.onComplete {
              case Success(config) =>
                self ! LoadConfiguration(config)
                s ! OperationResult(
                  Instant.now().toString,
                  "Success",
                  "LoadConfigurationURL",
                  "",
                  "",
                  None,
                  Some(s"Configuration from URL $configurationURL loaded"))
                log.info(s"INSTALLATION: Configuration from URL $configurationURL loaded!")
              case Failure(ex) =>
                log.error(s"INSTALLATION: LoadConfigurationURL ERROR parsing response: ${ex.getLocalizedMessage}!")
                s ! OperationResult(
                  Instant.now().toString,
                  "Failure",
                  "LoadConfigurationURL",
                  "",
                  "",
                  None,
                  Some(s"$configurationURL cannot be parsed as InstallationState: ${ex.getLocalizedMessage}"))
            }
          case Failure(ex) =>
            log.error(s"INSTALLATION: LoadConfigurationURL: ${ex.getLocalizedMessage}!")
            s ! OperationResult(
              Instant.now().toString,
              "Failure",
              "LoadConfigurationURL",
              "",
              "",
              None,
              Some(s"Failed to access $configurationURL : ${ex.getLocalizedMessage}"))
        }
      } else {
        sender() ! OperationResult(
          Instant.now().toString,
          "Failure",
          "LoadConfiguration too early!",
          "",
          "",
          None,
          None)
      }

    case AskStatusUpdate =>
      import akka.pattern.ask
      implicit val timeout: Timeout = Timeout(1 minute)

//      val maybeSeqStatus: Future[Iterable[Option[ApplicationComponent]]] = {
      Future.traverse(context.children) ( children =>
          (children ? GetState).mapTo[Option[ApplicationComponent]]
        ).onComplete{
        case Success(statuses) =>
          installationState = installationState match {
            case Some(installation) => Some(installation.copy(deployerParameters = Seq(), applicationComponents = statuses.asInstanceOf[Seq[Option[ApplicationComponent]]].flatten))
            case None => installationState
          }
        case Failure(ex)=> log.warning(s"INSTALLATION: AskStatusUpdate wait timeout after $timeout minute(s) with exception: ${ex.getLocalizedMessage}")
      }

    case GetInstallation =>
      sender() ! installationState

    case GetApplicationComponentState(component: String) =>
      if (installationState.isDefined) {
        try {
          log.debug(s"INSTALLATION: GetApplicationComponentState: ${component.toLowerCase} component count: ${installationState.get.applicationComponents.length}")
          val comp: Seq[ApplicationComponent] = installationState.get.applicationComponents.filter(comp => comp.name.equals(component))
          log.debug(s"INSTALLATION: GetApplicationComponentState: $comp")

          if (comp.nonEmpty) {
            log.debug(s"INSTALLATION: GetApplicationComponentState: $comp")
            sender ! Some(comp.head)
          }
          else {
            log.warning(s"INSTALLATION: GetApplicationComponentState: ${component.toLowerCase} not found!")
            sender ! None
          }
        } catch {
          case ex: Exception =>
            log.error(s"INSTALLATION: GetApplicationComponentState: ${ex.getLocalizedMessage}!")
            sender ! None
        }
      } else {
        log.warning(s"INSTALLATION: GetApplicationComponentState: no configuration found!")
        sender ! None
      }

    case GetApplicationComponentEvents(componentName) =>
      if (installationState.isDefined) {
        try {
          log.debug(s"INSTALLATION: GetApplicationComponentEvents: ${componentName.toLowerCase} component count: ${installationState.get.applicationComponents.length}")
          val comp: Seq[ApplicationComponent] = installationState.get.applicationComponents.filter(comp => comp.name.equals(componentName))
          log.debug(s"INSTALLATION: GetApplicationComponentEvents: $comp")

          if (comp.nonEmpty) {
            log.debug(s"INSTALLATION: GetApplicationComponentEvents: $comp")
            sender ! Some(OperationEvents(comp.head.events))
          }
          else {
            log.warning(s"INSTALLATION: GetApplicationComponentEvents: ${componentName.toLowerCase} not found!")
            sender ! None
          }
        } catch {
          case ex: Exception =>
            log.error(s"INSTALLATION: GetApplicationComponentEvents: ${ex.getLocalizedMessage}!")
            sender ! None
        }
      } else {
        log.warning(s"INSTALLATION: GetApplicationComponentEvents: no configuration found!")
        sender ! None
    }

    case GetApplicationComponentMetrics(componentName) =>
      if (installationState.isDefined) {
        try {
          log.debug(s"INSTALLATION: GetApplicationComponentMetrics: ${componentName.toLowerCase} component count: ${installationState.get.applicationComponents.length}")
          val comp: Seq[ApplicationComponent] = installationState.get.applicationComponents.filter(comp => comp.name.equals(componentName))
          log.debug(s"INSTALLATION: GetApplicationComponentMetrics: $comp")

          if (comp.nonEmpty) {
            log.debug(s"INSTALLATION: GetApplicationComponentMetrics: $comp")
            sender ! Some(Metrics(comp.head.metrics))
          }
          else {
            log.warning(s"INSTALLATION: GetApplicationComponentMetrics: ${componentName.toLowerCase} not found!")
            sender ! None
          }
        } catch {
          case ex: Exception =>
            log.error(s"INSTALLATION: GetApplicationComponentMetrics: ${ex.getLocalizedMessage}!")
            sender ! None
        }
      } else {
        log.warning(s"INSTALLATION: GetApplicationComponentMetrics: no configuration found!")
        sender ! None
      }

    case InitializeApplicationComponent(componentName) =>
      log.debug(s"INSTALLATION: InitializeApplicationComponent component: $componentName")
      sender() ! OperationResult(
        Instant.now().toString,
        "Accepted",
        "Initialization queued",
        "",
        "",
        None,
        None)

      installationState.get.applicationComponents.filter(appcomp => appcomp.name == componentName).foreach { component =>
        context.actorSelection(component.name.toLowerCase).resolveOne().onComplete {
          case Success(actor) => actor ! Initialize(component, installationState.get.controlParameters,installationState.get.deployerParameters)
          case Failure(_) =>
            val deployer = context.actorOf(Deployer.props, component.name)
            deployer ! Initialize(component, installationState.get.controlParameters,installationState.get.deployerParameters)
        }
      }

    case InitializeDeployers =>
      log.debug(s"INSTALLATION: InitializeDeployers")
      installationState.get.applicationComponents.foreach { component =>
        log.info(s"INSTALLATION: InitializeDeployers component: ${component.name}")
        context.actorSelection(component.name.toLowerCase).resolveOne().onComplete {
          case Success(deployer) =>
            deployer ! Initialize(component, installationState.get.controlParameters,installationState.get.deployerParameters)
          case Failure(_) =>
            log.info(s"INSTALLATION: InitializeDeployers creating new actor for: ${component.name}")
            val deployer = context.actorOf(Deployer.props, component.name.toLowerCase)
            deployer ! Initialize(component, installationState.get.controlParameters,installationState.get.deployerParameters)
        }
      }

    case GetAllPhases =>
      val localSender = sender
      phases match {
        case Some(ps) => (ps ? GetPhasesSeq).map(a => localSender ! a)
        case None => localSender ! PhasesSeq(Seq())
      }
  }
}