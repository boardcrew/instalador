package org.boardcrew.installer

import akka.NotUsed
import akka.actor.{Actor, ActorLogging, Props}
import akka.stream.ActorMaterializer
import akka.stream.alpakka.elasticsearch.WriteMessage
import akka.stream.alpakka.elasticsearch.scaladsl.ElasticsearchFlow
import akka.stream.scaladsl.{Sink, Source}
import com.typesafe.config.ConfigFactory
import org.apache.http.HttpHost
import org.boardcrew.installer.Deployer.GetState
import org.boardcrew.installer.Publisher.PublishEvent
import org.elasticsearch.client.RestClient

object Publisher {
  def props: Props = Props[Publisher]

  final case class PublishEvent(event: OperationEvent)

}

class Publisher extends Actor with ActorLogging with JsonSupport with PhasesUtils {

  implicit var clientLocal: Option[RestClient] = None

  override def preStart(): Unit = {
    val config = ConfigFactory.load()
    if (config.getString("instalador.heartbeatEnabled").toBoolean) {
      clientLocal = connectES
    } else {
      log.info(s"Publisher started without Elasticsearch connection!!")
    }
  }

  override def receive: Receive = {

    case PublishEvent(event) =>
      log.debug(s"Publisher ${event.componentName} publishing")
      val config = ConfigFactory.load()
      if (config.getString("instalador.heartbeatEnabled").toBoolean) {
        log.debug(s"Publisher publishing to Elasticsearch!")

        clientLocal match {
          case Some(cl) =>
            val requests = List[WriteMessage[OperationEvent, NotUsed]](WriteMessage.createIndexMessage(source = event))
            implicit val client: RestClient = cl
            implicit val materializer: ActorMaterializer = ActorMaterializer()
            Source(requests)
              .via(
                ElasticsearchFlow.create[OperationEvent](
                  "instalador",
                  "operationEvent"
                )
              )
              .runWith(Sink.seq)
          case None =>
            //TODO: define max number of retries
            log.debug(s"Trying to reconnect to Elasticsearch!")
            clientLocal = connectES
            self ! PublishEvent(event)
        }
      } else {
        log.debug(s"Publisher not publishing to Elasticsearch!")
      }

    case GetState => None

    case _ =>
      log.warning(s"Publisher received invalid message from ${sender.toString()}, message not accepted!")

  }

  def connectES: Option[RestClient] = {
    //TODO: ensure no Exception leave this block
    val config = ConfigFactory.load()
    log.info(s"Publisher started with connection to Elasticsearch at http://${config.getString("instalador.heartbeatHost")}:${config.getString("instalador.heartbeatPort").toInt}!")
    Some(RestClient.builder(
      new HttpHost(
        config.getString("instalador.heartbeatHost"),
        config.getString("instalador.heartbeatPort").toInt
      )).setMaxRetryTimeoutMillis(400).build()
    )
  }
}