package org.boardcrew.installer

import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import kamon.Kamon
import kamon.prometheus.PrometheusReporter
import kamon.system.SystemMetrics

import scala.concurrent.Await
import scala.concurrent.duration.Duration


object InstallerServer extends App with ApplicationRoutes {

  Kamon.addReporter(new PrometheusReporter())
  // https://github.com/kamon-io/kamon-system-metrics
  SystemMetrics.startCollecting()


  implicit val system: ActorSystem = ActorSystem("Instalador")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  val installation: ActorRef = system.actorOf(Installation.props, "installation")
  lazy val routes: Route = userRoutes
  Http().bindAndHandle(routes, "0.0.0.0", 8080)
  println(s"Instalador running on: http://0.0.0.0:8080/")
  Await.result(system.whenTerminated, Duration.Inf)
}

