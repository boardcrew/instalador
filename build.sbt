import com.typesafe.sbt.packager.docker.{Cmd, ExecCmd}

lazy val akkaHttpVersion = "10.1.3"
lazy val akkaVersion    = "2.5.14"
lazy val kubectlVersion = "1.13.0"
lazy val lightbendRPVersion = "1.6.0"
lazy val helmVersion = "2.12.1"
lazy val scalateVersion = "1.8.0"
lazy val log4jVersion = "1.7.5"
lazy val jqVersion = "1.6"

// https://kamon.io/documentation/1.x/recipes/adding-the-aspectj-weaver/#deploying-with-sbt-native-packager

enablePlugins(JavaAppPackaging,JavaAgent)
javaAgents += "org.aspectj" % "aspectjweaver" % "1.8.13"
javaOptions in Universal += "-Dorg.aspectj.tracing.factory=default"

lazy val instalador = (project in file(".")).
  settings(
    inThisBuild(List(
      organization    := "org.boardcrew.installer",
      scalaVersion    := "2.12.4",
      version         := "0.1.38"
    )),
    packageName in Docker := "instalador",
    dockerBaseImage       := "openjdk:8-jre-alpine",
    dockerCommands :=
      dockerCommands.value.flatMap {
        case ExecCmd("ENTRYPOINT", args@_*) => Seq(Cmd("ENTRYPOINT", args.mkString(" ")))
        case c@Cmd("FROM", _) => Seq(c,
          //TODO: dnsutils
          ExecCmd("RUN", "/bin/sh", "-c", "apk add --no-cache bash git docker curl  && ln -sf /bin/bash /bin/sh"),
          ExecCmd("RUN", "/bin/sh", "-c", s"curl -LO https://storage.googleapis.com/kubernetes-release/release/v$kubectlVersion/bin/linux/amd64/kubectl && chmod +x ./kubectl && mv ./kubectl /usr/local/bin/kubectl"),
          ExecCmd("RUN", "/bin/sh", "-c", s"curl -L -o reactive-cli-$lightbendRPVersion-Linux-amd64.tar.gz https://bintray.com/lightbend/generic/download_file?file_path=reactive-cli-$lightbendRPVersion-Linux-amd64.tar.gz && tar xvzf reactive-cli-$lightbendRPVersion-Linux-amd64.tar.gz && chmod +x ./rp && mv ./rp /usr/local/bin/rp"),
          ExecCmd("RUN", "/bin/sh", "-c", s"curl -L -o /tmp/helm.tgz https://storage.googleapis.com/kubernetes-helm/helm-v$helmVersion-linux-amd64.tar.gz && tar xzf /tmp/helm.tgz linux-amd64/helm && mv linux-amd64/helm /usr/local/bin/helm && rmdir linux-amd64 && chmod 755 /usr/local/bin/helm && rm /tmp/helm.tgz"),
          ExecCmd("RUN", "/bin/sh", "-c", s"mkdir -p /opt/instalador ; chown daemon:daemon /opt/instalador"),
          ExecCmd("RUN", "/bin/sh", "-c", s"curl -L -o /usr/local/bin/envsubst https://github.com/a8m/envsubst/releases/download/v1.1.0/envsubst-Linux-x86_64 && chmod +x /usr/local/bin/envsubst"),
          ExecCmd("RUN", "/bin/sh", "-c", s"curl -L -o /usr/local/bin/jq https://github.com/stedolan/jq/releases/download/jq-$jqVersion/jq-linux64 && chmod +x /usr/local/bin/jq")
        )
        case v => Seq(v)
      },
    dockerExposedPorts := Seq(8080,9095),
    name := "instalador",
    libraryDependencies ++= Seq(
      "com.typesafe.akka"        %% "akka-http"                         % akkaHttpVersion,
      "com.typesafe.akka"        %% "akka-http-spray-json"              % akkaHttpVersion,
      "com.typesafe.akka"        %% "akka-http-xml"                     % akkaHttpVersion,
      "com.typesafe.akka"        %% "akka-stream"                       % akkaVersion,
      "com.typesafe.akka"        %% "akka-slf4j"                        % akkaVersion,
      "org.scalatra.scalate"     %% "scalate-core"                      % scalateVersion,
      "ch.qos.logback"           %  "logback-classic"                   % "1.2.3",
      "com.github.danielwegener" %  "logback-kafka-appender"            % "0.2.0-RC1",
      "org.slf4j"                %  "jul-to-slf4j"                      % "1.7.25",
      "io.skuber"                %% "skuber"                            % "2.0.9",
      "com.lightbend.akka"       %% "akka-stream-alpakka-elasticsearch" % "1.0-M1",
      "io.kamon"                 %% "kamon-core"                        % "1.1.3",
      "io.kamon"                 %% "kamon-system-metrics"              % "1.0.0",
      "io.kamon"                 %% "kamon-akka-2.5"                    % "1.1.2",
      "io.kamon"                 %% "kamon-akka-http-2.5"               % "1.1.1",
      "io.kamon"                 %% "kamon-prometheus"                  % "1.1.0",
      "com.typesafe.akka"        %% "akka-http-testkit"                 % akkaHttpVersion % Test,
      "com.typesafe.akka"        %% "akka-testkit"                      % akkaVersion     % Test,
      "com.typesafe.akka"        %% "akka-stream-testkit"               % akkaVersion     % Test,
      "org.scalatest"            %% "scalatest"                         % "3.0.5"         % Test
    ),
    mainClass := Some("org.boardcrew.installer.InstallerServer")
  )
